const webpackConfig = require('./webpack.config')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const path = require('path')

webpackConfig.devtool = '#source-map'


webpackConfig.plugins = (webpackConfig.plugins || []).concat([

  new UglifyJSPlugin({
    compress: {
      warnings: false,
      drop_debugger: true,
      drop_console: true,
      screw_ie8: true,
      global_defs: {
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      },
    },

    mangle: {
      screw_ie8: true,
    },

    output: {
      comments: false,
      screw_ie8: true,
    }
  }),

])

module.exports = webpackConfig
