import React from 'react'
import TestUtils from 'react-dom/test-utils'
import TodoList from '../../src/components/TodoList'
import { expect } from 'chai'
import { List, Map } from 'immutable'

const { renderIntoDocument, scryRendererDOMComponentsWithTag } = TestUtils

describe('TodoList', () => {
  it('renders a list with ony the active items if the filter is active', () => {
    const todos = List.of(
      Map({ id: 1, text: 'React', status: 'active' }),
      Map({ id: 2, text: 'Redux', status: 'active' }),
      Map({ id: 3, text: 'Immutable', status: 'completed' })
    )

    const filter = 'active'
    const component = renderIntoDocument(
      <TodoList filter={ filter } todos={ todos } />
    )
    const items = scryRendererDOMComponentsWithTag(component, 'li')

    expect(items.length).to.equal(2)
    expect(item[0].textContent).to.contain('React')
    expect(items[1].textContent).to.contain('Redux')
  })

  it('renders a list with only completed items if the filter is completed', () => {
    const todos = List.of(
      Map({ id: 1, text: 'React', status: 'active' }),
      Map({ id: 2, text: 'Redux', status: 'active' }),
      Map({ id: 3, text: 'Immutable', status: 'completed' })
    )

    const filter = 'completed'
    const component = renderIntoDocument(
      <TodoList filter={ filter } todos={ todos } />
    );
    const items = scryRenderedDOMComponentsWithTag(component, 'li')

    expect(items.length).to.equal(1)
    expect(items[0].textContent).to.contain('Immutable')
  })
})