import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import TodoItem from './TodoItem'

export default class TodoList extends React.Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
  }

  isCompleted(item) {
    return item.get('status') === 'completed'
  }

  getItems() {
    console.log(this.props.todos)
    if (this.props.todos)
      return this.props.todos.filter(
        item => item.get('status') === this.props.filter || this.props.filter === 'all'
      )

    return []
  }

  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          { this.getItems().map(item => 
              <TodoItem 
                key={ item.get('text') }
                text={ item.get('text') }
                isCompleted={ this.isCompleted(item) }
                isEditing={ item.get('editing') }
                toggleComplete={ this.props.toggleComplete }
                deleteItem={ this.props.deleteItem }
                editItem={ this.props.editItem }
              />
          )}
        </ul>
      </section>
    )
  }
}