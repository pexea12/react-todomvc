import React from 'react'
import { connect } from 'react-redux'

import TodoHeader from './TodoHeader'
import TodoTools from './TodoTools'
import TodoList from './TodoList'
import Footer from './Footer'

import * as actionCreators from '../action_creators'

export class TodoApp extends React.Component {
  getNbActiveItems() {
    return this.props.todos || []
  }

  render() {
    return (
      <div>
        <section className="todoapp">
          <TodoHeader addItem={ this.props.addItem } />
          <TodoList {...this.props} />
          <TodoTools
            changeFilter={ this.props.changeFilter }
            filter={ this.props.filter }
            nbActiveItems={ this.getNbActiveItems() }
            clearCompleted={ this.props.clearCompleted }
          />
        </section>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.get('todos'),
    filter: state.get('filter'),
  }
}

export const TodoAppContainer = connect(mapStateToProps, actionCreators)(TodoApp)